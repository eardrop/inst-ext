// Было много способов сделать скачку картинок. Конвертить в base64 и тд. Но так как запросы с картинок и видео идут не на ориджин  
// я решил интерсептить запросы и добавлять им content-disposition хедер чтобы можно было сделать супер простую скачку. Ну и плюс я такой подход 
// особо не использовал и хотелось еще раз его попробовать. Не скажу что этот вариант самый future-proof так как хосты могут
// менятся и тд.

var contentDispositionHeader = {
  name: 'content-disposition',
  value: 'attachment'
};

chrome.webRequest.onHeadersReceived.addListener(
  function(details) {
    if (details.url.indexOf('https://instagram.') == 0 || details.url.indexOf('https://scontent.') == 0) {
      details.responseHeaders.push(contentDispositionHeader);
      return { 
        responseHeaders: details.responseHeaders
      };
    }
  },
  {urls: ['*://*/*']},
  ['blocking', 'responseHeaders']
);