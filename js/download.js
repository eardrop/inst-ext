// Изначально я думал следить за результатом обзёрвера, смотреть какие ноды были добавлены и 
// проверять подходящие ли это элементы. плюс отдельно на ините смотреть что есть на странице и тд.
// но в итоге решил что можно сделать куда проще и просто при ините и мутэйшине апдейтить 
// все элементы с srcset (что покрывает почти все кейсы) и для открытых видео постов искать по 
// аттрибуту poster. К тому же это покрывает explore/search/feeds страницы.
// Ну и плюс цеплять флаг атрибут download-added чтобы не апдейтить один и тотже дважды


const flagAttribute = 'download-added';
const excludeFlagSelector = ':not([' + flagAttribute + '])';

const imageSelector = 'img[srcset]' + excludeFlagSelector;
const videoSelector = '[poster]'  + excludeFlagSelector;

const observeElement = document.body;
const observeConfig = {
  childList: true,
	subtree: true,
  attributes: true
};

function getList (type) {
  return document.querySelectorAll(type == 'image' ? imageSelector : videoSelector);
}

function getDownloadButton (item) {
  var link = item.src;
  var button = document.createElement('a');
  button.innerHTML = 'Download';
  button.className = 'download-button';
  button.setAttribute('href', link);
  button.setAttribute('download', true);
  return button;
}

function attachButton (item, button) {
  if (item.closest('nav')) return;
  var parentLinkTag = item.closest('a');
  if (parentLinkTag) {
    parentLinkTag.parentNode.insertBefore(button, parentLinkTag);
  } else {
    if (item.parentNode.parentNode.tagName == 'SECTION') return; // это хак против авто удаления элементов во вью на странице фидов 
    item.parentNode.insertBefore(button, item);
  }
}

function addFlag (item) {
  item.setAttribute(flagAttribute, true);
}

function updateElements (list) {
  list.forEach(function (item) {
    var dowloadButton = getDownloadButton(item);
    attachButton(item, dowloadButton);
    addFlag(item);
  })
}

function findAndUpdateElements (type) {
  updateElements(nodeListToArray(getList('image')));
  updateElements(nodeListToArray(getList('video')));
}

const observer = new MutationObserver(findAndUpdateElements);
observer.observe(observeElement, observeConfig);

findAndUpdateElements();
